package com.mycomp.practicetodo;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@SpringBootApplication
@OpenAPIDefinition(info =  @Info(title = "Product API", version = "${app.version}", description = "Test API"))
public class PracticeToDoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PracticeToDoApplication.class, args);
    }



}
