package com.mycomp.practicetodo.task;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

public class TaskDTO implements Serializable {

    @Size(min = 3, max = 30, message = "{message.error}")
    private String name;

    public TaskDTO() {

    }

    public TaskDTO(String name) {
        this.name = name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDTO entity = (TaskDTO) o;
        return Objects.equals(this.name, entity.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "name = " + name + ")";
    }
}
