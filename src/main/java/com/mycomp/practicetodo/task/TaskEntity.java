package com.mycomp.practicetodo.task;

import com.mycomp.practicetodo.TaskStatus;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(EnumType.ORDINAL)
    TaskStatus taskStatus;

    String name;
    LocalDateTime creatingTime;

    public Integer getId() {
        return id;
    }

    public TaskStatus getStatus() {
        return taskStatus;
    }

    public void setStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatingTime() {
        return creatingTime;
    }

    public void setCreatingTime(LocalDateTime creatingTime) {
        this.creatingTime = creatingTime;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
