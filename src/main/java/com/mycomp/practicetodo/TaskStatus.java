package com.mycomp.practicetodo;

public enum TaskStatus {
    Planned,
    Work_in_progress,
    Done,
    Cancelled
}
