package com.mycomp.practicetodo.repos;

import com.mycomp.practicetodo.TaskStatus;
import com.mycomp.practicetodo.task.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ToDoRepository extends JpaRepository<TaskEntity, Integer> {

    @Query("select t from TaskEntity t where t.taskStatus = ?1 or t.taskStatus = ?2")
    List<TaskEntity> getAllByStatuses(TaskStatus status1, TaskStatus status2);
    TaskEntity getFirstByName(String name);
}
