package com.mycomp.practicetodo.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycomp.practicetodo.task.TaskDTO;
import com.mycomp.practicetodo.task.TaskEntity;
import com.mycomp.practicetodo.services.ToDoService;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/todo")
public class ToDoController {

    ObjectMapper objectMapper = new ObjectMapper();
    Logger logger = LoggerFactory.getLogger(ToDoController.class);
    @Autowired
    ToDoService toDoService;

    @GetMapping("/all")
    @Operation(summary = "Get all tasks", description = "Returning list of all created tasks (including ended)")
    public List<TaskEntity> getAllTasks() {
        logger.debug("Calling 'getAllTasks' method");
        return toDoService.getAllTasks();
    }

    ;

    @GetMapping("/allnotended")
    @Operation(summary = "Get all not ended tasks",
            description = "Returning list of all tasks with status \"created\" and \"work_in_progress\"")
    public List<TaskEntity> getAllNotEnded() {
        logger.debug("Calling 'getAllNotEnded method");
        return toDoService.getAllNotEnded();
    }

    ;

    @GetMapping("/allended")
    @Operation(summary = "Get all ended tasks",
            description = "Returning list of all tasks with status \"done\" and \"cancelled\"")
    public List<TaskEntity> getAllEnded() {
        logger.debug("Calling 'getAllEnded' method");
        return toDoService.getAllEnded();
    }

    @PostMapping("/")
    @Operation(summary = "Creating new task",
            description = "Creating new task. If name is not valid - it won`t be created")
    public String addTask(@RequestBody @Valid TaskDTO task, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<FieldError> errors = bindingResult.getFieldErrors();
            StringBuilder sb = new StringBuilder("[");
            for (FieldError error : errors) {
                logger.info(error.getDefaultMessage());
                sb.append("\"").append(error.getDefaultMessage()).append(" in the field '")
                        .append(error.getField()).append("'\",\t");
            }
            return sb.delete(sb.length()-2, sb.length()).append("]").toString();
        } else {
            toDoService.addTask(task);
            logger.debug("Adding new entity with name '" + task.getName() + "'");
            return "Added";
        }

    }

    @PutMapping("/cancel")
    @Operation(summary = "Setting task status to \"Canceled\"",
            description = "If status of task is \"Planned\" or \"Work in progress\" it will be changed to \"Canceled\". " +
                    "Other statuses won`t be changed")
    public String cancelTask(@RequestBody TaskDTO task) {
        logger.debug("Cancelling task with name '" + task.getName() + "'");
        return toDoService.cancelTask(task);
    }

    @PutMapping("/update")
    @Operation(summary = "Changing status of task",
            description = "If status was \"Planned\" it will be changet to \"Work in progress\". " +
                    "If \"Work in progress\" changing to \"Done\". Other statuses won`t be changed")
    public String updateTask(@RequestBody TaskDTO task) {
        logger.debug("Updating task with name '" + task.getName() + "'");
        return toDoService.updateTask(task);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Deleting task ",
            description = "Deleting task if exist")
    public String deleteTask(@PathVariable Integer id){
        logger.debug("Deleting task with id [" +id +"]");
        return toDoService.deleteTask(id);
    }


}
