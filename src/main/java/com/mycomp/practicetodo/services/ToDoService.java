package com.mycomp.practicetodo.services;

import com.mycomp.practicetodo.TaskStatus;
import com.mycomp.practicetodo.repos.ToDoRepository;
import com.mycomp.practicetodo.task.TaskDTO;
import com.mycomp.practicetodo.task.TaskEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class ToDoService {

    Logger logger = LoggerFactory.getLogger(ToDoService.class);
    @Autowired
    ToDoRepository toDoRepository;

    public void addTask(TaskDTO task) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setName(task.getName());
        taskEntity.setStatus(TaskStatus.Planned);
        taskEntity.setCreatingTime(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        toDoRepository.save(taskEntity);
    }

    public List<TaskEntity> getAllTasks() {
        return toDoRepository.findAll();
    }

    public List<TaskEntity> getAllEnded() {
        return toDoRepository.getAllByStatuses(TaskStatus.Done, TaskStatus.Cancelled);
    }

    public List<TaskEntity> getAllNotEnded() {
        return toDoRepository.getAllByStatuses(TaskStatus.Planned, TaskStatus.Work_in_progress);
    }

    public String cancelTask(TaskDTO task) {
        TaskEntity taskEntity = toDoRepository.getFirstByName(task.getName());
        if (taskEntity != null && (taskEntity.getStatus()==TaskStatus.Planned || taskEntity.getStatus()==TaskStatus.Work_in_progress)) {
            taskEntity.setStatus(TaskStatus.Cancelled);
            toDoRepository.save(taskEntity);
            logger.debug("Task '" + task.getName() + "' was cancelled");
            return "[\"Task '" + task.getName() + "' was cancelled\"]";
        } else if(taskEntity != null && (taskEntity.getStatus()==TaskStatus.Done || taskEntity.getStatus()==TaskStatus.Cancelled)) {
            logger.debug("Task '" + task.getName() + "' can`t be cancelled because it`s status alreade " + taskEntity.getStatus());
            return "[\"Task '" + task.getName() + "' can`t be cancelled because it`s status alreade " + taskEntity.getStatus() + "\"]";
        }else {
            logger.warn("No task '" + task.getName() + "' in database");
            return "[\"No task '" + task.getName() + "' in database\"]";
        }
    }

    public String updateTask(TaskDTO task){
        TaskEntity taskEntity = toDoRepository.getFirstByName(task.getName());
        if (taskEntity != null) {
            switch (taskEntity.getStatus()){
                case Planned:{
                    taskEntity.setStatus(TaskStatus.Work_in_progress);
                    toDoRepository.save(taskEntity);
                    logger.debug("Task was updated from " + TaskStatus.Planned + " to " + TaskStatus.Work_in_progress);
                    return "[\"Task was updated from " + TaskStatus.Planned + " to " + TaskStatus.Work_in_progress + "\"]";
                }
                case Work_in_progress:{
                    taskEntity.setStatus(TaskStatus.Done);
                    toDoRepository.save(taskEntity);
                    logger.debug("Task was updated from " + TaskStatus.Work_in_progress + " to " + TaskStatus.Done);
                    return "[\"Task was updated from " + TaskStatus.Work_in_progress + " to " + TaskStatus.Done + "\"]";
                }
                default:{
                    logger.warn("Task status is already "+ taskEntity.getStatus());
                    return "[\"Task status is already "+ taskEntity.getStatus() + "\"]";
                }
            }
        } else {
            logger.warn("No task '" + task.getName() + "' in database");
            return "[\"No task '" + task.getName() + "' in database\"]";
        }
    }

    public String deleteTask(Integer id){
        TaskEntity task = toDoRepository.findById(id).orElse(null);
        if(task!=null){
            toDoRepository.deleteById(id);
            logger.debug("Task '" + task.getName() + "' was deleted");
            return "[\"Task '" + task.getName() + "' was deleted\"]";
        }else {
            logger.debug("No task to delete");
            return "[\"No task to delete\"]";
        }

    }
}
