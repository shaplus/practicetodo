package com.mycomp.practicetodo;

import com.mycomp.practicetodo.controllers.ToDoController;
import com.mycomp.practicetodo.services.ToDoService;
import com.mycomp.practicetodo.task.TaskEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PracticeToDoApplication.class)
@AutoConfigureMockMvc

class PracticeToDoApplicationTests {

    @Mock
    ToDoService service;
    @InjectMocks
    ToDoController controller;




    @Test
    private void testAdding(){
        when(service.getAllTasks()).thenReturn(new ArrayList<>());
        List<TaskEntity> list = controller.getAllTasks();
        verify(service).getAllTasks();
    }

}
