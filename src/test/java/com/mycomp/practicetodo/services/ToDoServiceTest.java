package com.mycomp.practicetodo.services;

import com.mycomp.practicetodo.TaskStatus;
import com.mycomp.practicetodo.repos.ToDoRepository;
import com.mycomp.practicetodo.task.TaskDTO;
import com.mycomp.practicetodo.task.TaskEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class ToDoServiceTest {

    TaskEntity entity = new TaskEntity();

    @BeforeEach
    void init(){
        entity.setName("Task1");
        entity.setStatus(TaskStatus.Planned);
        entity.setId(1);
        entity.setCreatingTime(LocalDateTime.now());
    }
    @MockBean
    ToDoRepository repository;

@InjectMocks
    @Autowired
    ToDoService toDoService;

    @Test
    void addTask() {
        toDoService.addTask(new TaskDTO("Task1"));
        verify(repository, times(1)).save(any());
    }

    @Test
    void getAllTasks() {
        toDoService.getAllTasks();
        verify(repository,times(1)).findAll();
    }

    @Test
    void getAllEnded() {
        toDoService.getAllEnded();
        verify(repository, times(1)).getAllByStatuses(TaskStatus.Done, TaskStatus.Cancelled);
    }

    @Test
    void getAllNotEnded() {
        toDoService.getAllNotEnded();
        verify(repository, times(1)).getAllByStatuses(TaskStatus.Planned, TaskStatus.Work_in_progress);
    }

    @Test
    void cancelTask() {
        when(repository.getFirstByName("Task1")).thenReturn(entity);
        assertEquals("[\"Task '" + entity.getName() + "' was cancelled\"]", toDoService.cancelTask(new TaskDTO("Task1")));
        entity.setStatus(TaskStatus.Done);
        assertEquals("[\"Task '" + entity.getName() + "' can`t be cancelled because it`s status alreade " + entity.getStatus() + "\"]",
                toDoService.cancelTask(new TaskDTO("Task1")));
        when(repository.getFirstByName("Task1")).thenReturn(null);
        assertEquals("[\"No task '" + entity.getName() + "' in database\"]", toDoService.cancelTask(new TaskDTO("Task1")));
    }

    @Test
    void updateTask() {
        when(repository.getFirstByName("Task1")).thenReturn(entity);
        assertEquals("[\"Task was updated from " + TaskStatus.Planned + " to " + TaskStatus.Work_in_progress + "\"]",
                toDoService.updateTask(new TaskDTO("Task1")));
        entity.setStatus(TaskStatus.Work_in_progress);
        assertEquals("[\"Task was updated from " + TaskStatus.Work_in_progress + " to " + TaskStatus.Done + "\"]",
                toDoService.updateTask(new TaskDTO("Task1")));
        entity.setStatus(TaskStatus.Done);
        assertEquals("[\"Task status is already "+ entity.getStatus() + "\"]", toDoService.updateTask(new TaskDTO("Task1")));
        entity.setStatus(TaskStatus.Cancelled);
        assertEquals("[\"Task status is already "+ entity.getStatus() + "\"]", toDoService.updateTask(new TaskDTO("Task1")));
        when(repository.getFirstByName("Task1")).thenReturn(null);
        assertEquals("[\"No task '" + entity.getName() + "' in database\"]", toDoService.updateTask(new TaskDTO("Task1")));

    }

    @Test
    void deleteTask() {
        when(repository.findById(1)).thenReturn(Optional.of(entity));
        assertEquals("[\"Task '" + entity.getName() + "' was deleted\"]", toDoService.deleteTask(1));
        when(repository.findById(1)).thenReturn(Optional.empty());
        assertEquals("[\"No task to delete\"]", toDoService.deleteTask(1));
    }
}