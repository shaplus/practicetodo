package com.mycomp.practicetodo;

import com.mycomp.practicetodo.TaskStatus;
import com.mycomp.practicetodo.repos.ToDoRepository;
import com.mycomp.practicetodo.task.TaskEntity;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ToDoControllerTest {
    @Autowired
    ToDoRepository toDoRepository;




        @Test
        public void addingTest(){
            TaskEntity task = new TaskEntity();
            task.setStatus(TaskStatus.Planned);
            task.setName("Task1");
            task.setCreatingTime(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
            assertEquals(0, toDoRepository.findAll().size());
            toDoRepository.save(task);
            assertEquals(1, toDoRepository.findAll().size());
        }

        @Test
        public void updateTest(){
            TaskEntity task = new TaskEntity();
            task.setStatus(TaskStatus.Planned);
            task.setName("Task1");
            task.setCreatingTime(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
            toDoRepository.save(task);
            int id = task.getId();
            assertEquals(TaskStatus.Planned, toDoRepository.getById(id).getStatus());
            task.setStatus(TaskStatus.Work_in_progress);
            toDoRepository.save(task);
            assertEquals(TaskStatus.Work_in_progress, toDoRepository.getById(id).getStatus());
            task.setStatus(TaskStatus.Done);
            toDoRepository.save(task);
            assertEquals(TaskStatus.Done, toDoRepository.getById(id).getStatus());
        }

        @Test
    public void cancelingTest(){
            TaskEntity task = new TaskEntity();
            task.setStatus(TaskStatus.Planned);
            task.setName("Task1");
            task.setCreatingTime(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
            toDoRepository.save(task);
            int id = task.getId();
            task.setStatus(TaskStatus.Cancelled);
            toDoRepository.save(task);
            assertEquals(TaskStatus.Cancelled, toDoRepository.getById(id).getStatus());
        }


}